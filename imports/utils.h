/*
 *  Author  : Philippe Pitz Clairoux
 *  Date    : 14 mai 2018
 *  Details : This file contains the prototypes that are defined in utils.cpp . All these functions are related to
 *  parsing args and the loading bar function as well.
 */
#ifndef FINAL_PROJECT_UTILS_H
#define FINAL_PROJECT_UTILS_H

#include "dataStructures.h"

namespace utils {

    [[ noreturn ]] void c_exit(const std::string &message, int errorCode) noexcept;

    void arg_parser(int argc, char **argv, structures::ParsedArgs &arg);

    void progress_bar(structures::PortQueue *scannedPorts, const unsigned long &amountPorts) noexcept;

    void load_ports(std::map<std::string, std::string> &toFill, const std::string &FileName);

}
#endif //FINAL_PROJECT_UTILS_H
