/*
 *  Author  : Philippe Pitz Clairoux
 *  Date    : 14 mai 2018
 *  Details : This file contains the definitions of networkCalls.cpp . It basically only contains the functions that
 *  are public. This is why the class Client isn't declared inside this headerfile.
 */
#ifndef FINAL_PROJECT_NETWORKCALLS_H
#define FINAL_PROJECT_NETWORKCALLS_H


#include "dataStructures.h"

namespace network {

    const unsigned short DEFAULT_PORTS[] ={20, 21, 22, 23, 25, 53, 67, 68, 69, 80, 110, 113, 123, 137, 138, 139, 143,
                                           161, 162, 179, 389, 443, 465, 587, 636, 666, 843, 989, 990, 993, 2526, 2525,
                                           5222, 8008, 8010, 9090, 9100};


    void test_port(structures::PortQueue *portsToCheck, structures::PortQueue *workingPorts,
                   structures::PortQueue *scannedPorts,
                   structures::ParsedArgs *args, std::map<std::string, std::string> PortInfo);

}
#endif //FINAL_PROJECT_NETWORKCALLS_H
