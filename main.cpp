/*
 *  Author  : Philippe Pitz Clairoux
 *  Date    : 14 mai 2018
 *  Details : This file is the main folder. From this file, we call the function to parse args, we create new instances
 *  of PortQueues to keep track of how many ports were scanned and to hold the ones that need to be scanned.
 *  We also create threads and shove them in a thread pool. This file also handles the printing of open ports.
 */
#include <boost/asio/io_service.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <aalib.h>
#include <iomanip>
#include <iostream>


#include "imports/networkCalls.h"
#include "imports/utils.h"

int main(int argc, char* argv[]) {

  structures::PortQueue portsToTest;
  structures::PortQueue openPorts;
  structures::PortQueue scannedPorts;
  boost::asio::io_service ioService;
  boost::thread_group threadpool;
  auto *args = new structures::ParsedArgs;
  std::string agree;
  std::map<std::string, std::string>PortInfo;

  //init the program and do the initial testing
    utils::load_ports(PortInfo, "ports.txt");
    utils::arg_parser(argc, argv, *args);

  //if --skip-warning is used, skip warning
  if (!args->SkipWarning) {
      std::cout << "Please make sure you have concent to use this program on a website before doing so." << std::endl;
      std::cout << "This program does alot of requests/sec, it might be consideredas DoS/DDoS by the targetted website."
                << std::endl;
      std::cout << "By clicking enter, you consent that the creator of this program is not responsible for the"
                   " damage you deal." << std::endl;

      std::getline(std::cin, agree);
  } else {
      std::cout << "Warning skipped." << std::endl;
  }

  std::cout << "Starting scan..." << std::endl;

  if (args->UseDefaultPorts) {
      if (args->Verbose)
          std::cout << "35 most popular ports are being scanned" << std::endl;

      for (unsigned short i : network::DEFAULT_PORTS)
          portsToTest.push(structures::_port(i, structures::port_state::default_value));
  }
  else if (args->Ports.empty()) {
      if (args->Verbose)
          std::cout << "Ports from 1 to 1024 are being scanned" << std::endl;
      for (unsigned short i = 1; i <= 1024; i++)
          portsToTest.push(structures::_port(i, structures::port_state::default_value));
  } else {
      if (args->Verbose)
          std::cout << "Ports you've manually inputted are being scanned" << std::endl;
      for (unsigned short Port : (args->Ports))
          portsToTest.push(structures::_port(Port, structures::port_state::default_value));
  }
  const int cores = boost::thread::hardware_concurrency();
  const unsigned long amountPorts = portsToTest.queueSize();

  assert(amountPorts > 0 && "There's 0 ports to scan. Not supposed to happen this far in the program.");

  if (args->Verbose)
    std::cout << "Currently have access to : " << cores << " cores" << std::endl;

  //create 1 thread per core
  for (int i = 0; i < 50; i++)
      threadpool.create_thread(boost::bind(network::test_port, &portsToTest, &openPorts, &scannedPorts, args, PortInfo));

  if (!args->Verbose)
    threadpool.create_thread(boost::bind(utils::progress_bar, &scannedPorts, amountPorts));


  portsToTest.run();
  //wait for every thread to be done.
  threadpool.join_all();

  //if all the ports were scanned, print the ones that are open (if there are any)!
  if (openPorts.queueSize() > 0) {
    std::cout << "Open ports : " << std::endl;
    openPorts.run();
    std::cout << std::left << std::setw(20) << "Target" << std::setw(20) << "_port" << std::setw(20)
              << "Service Name" << std::setw(20) << "State" << std::endl;
    while (openPorts.queueSize() != 0){
        auto currentPort = openPorts.pop();
        std::string proto = std::to_string(currentPort.Port) + "/" + args->Protocol;
        auto serviceToTest = PortInfo.find(proto);

        if (serviceToTest == PortInfo.end())
            serviceToTest->second = "unassigned";

        std::cout << std::left << std::setw(20) << args->Target << std::setw(20) << proto << std::setw(20)
                  << serviceToTest->second << std::setw(20) << (currentPort.State != 1 ? "OPEN" : "CLOSED") << std::endl;
    }

  } else
        std::cout << "The host you've inputted has no open ports." << std::endl;


  return 0;
}