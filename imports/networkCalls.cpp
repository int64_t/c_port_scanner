/*
 *  Author  : Philippe Pitz Clairoux
 *  Date    : 14 mai 2018
 *  Details : This file handles everything related to networking. It defines the prototypes from networkCalls.h and also
 *  creates a class named "client" wich will handle tcp connections. There's also a test_connection() function wich is
 *  used at the start of the program to see if you have access to the internet.
*/
#ifndef FINAL_PROJECT_NETWORKCALLS_CPP
#define FINAL_PROJECT_NETWORKCALLS_CPP

#define _REQUEST_TYPE "HTTP/1.0"

#include <boost/asio/deadline_timer.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/connect.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/thread.hpp>
#include <boost/asio/error.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <iostream>

#include "utils.h"
#include "networkCalls.h"


using boost::asio::ip::tcp;

class client
{
public:
    boost::system::error_code error;
    bool done = false;
    //constructor to start the connection and test the port
    client(boost::asio::io_service& io_service, const std::string& server, const std::string& port, const int16_t time)
            : resolver_(io_service), socket_(io_service), deadline_(io_service) {


        deadline_.async_wait(boost::bind(&client::check_deadline, this));
        deadline_.expires_from_now(boost::posix_time::seconds(time));
        tcp::resolver::query query(server, port);
        resolver_.async_resolve(query, boost::bind(&client::handle_resolve, this, boost::asio::placeholders::error,
                                                   boost::asio::placeholders::iterator));
    }



private:
    void stop(){
        this->done  = true;
        this->socket_.close();
        this->deadline_.cancel();
    }
    void check_deadline()
    {
        if (done)
            return;

        // Check whether the deadline has passed. We compare the deadline against
        // the current time since a new asynchronous operation may have moved the
        // deadline before this actor had a chance to run.
        if (deadline_.expires_at() <= boost::asio::deadline_timer::traits_type::now())
        {
            // The deadline has passed. The socket is closed so that any outstanding
            // asynchronous operations are cancelled.
            socket_.close();
            error = boost::asio::error::timed_out;
            stop();
            return;
        }

        // Put the actor back to sleep.
        deadline_.async_wait(boost::bind(&client::check_deadline, this));
    }
    void handle_resolve(const boost::system::error_code& err,
                        tcp::resolver::iterator endpoint_iterator)
    {
        if (!err && !done)
        {
            // Attempt a connection to the first endpoint in the list. Each endpoint
            // will be tried until we successfully establish a connection.
            tcp::endpoint endpoint = *endpoint_iterator;
            socket_.async_connect(endpoint, boost::bind(&client::handle_connect, this,
                                  boost::asio::placeholders::error, ++endpoint_iterator));

        }
        else
        {
            client::stop();
            if (error == boost::system::error_code())
                this->error = err;
        }
    }

    void handle_connect(const boost::system::error_code& err,
                        tcp::resolver::iterator endpoint_iterator)
    {
        if (!err && !done)
        {
            //connection work, change nothing
            this->error = boost::system::error_code();
            client::stop();
        }
        else if (endpoint_iterator != tcp::resolver::iterator() && !done)
        {
            // The connection failed. Try the next endpoint in the list.
            socket_.close();
            tcp::endpoint endpoint = *endpoint_iterator;
            socket_.async_connect(endpoint, boost::bind(&client::handle_connect, this,
                                              boost::asio::placeholders::error, ++endpoint_iterator));
        }
        else
        {
            client::stop();
            if (error == boost::system::error_code())
                this->error = err;
        }
    }

    tcp::resolver resolver_;
    tcp::socket socket_;
    boost::asio::deadline_timer deadline_;

};
//test a port
void network::test_port(structures::PortQueue *portsToCheck, structures::PortQueue *workingPorts,
                        structures::PortQueue *scannedPorts, structures::ParsedArgs *args,
                        std::map<std::string, std::string> PortInfo) {

    structures::_port currentPort;
    currentPort = portsToCheck->pop();
    //get a new port after each iteration. Do it as long that the port is bigger than 0
    while (currentPort.Port != 0){
        //format the port in order to see the default service assigned to it
        std::string proto = std::to_string(currentPort.Port) + "/" + args->Protocol;

        try {
            using namespace boost::asio::ip;
            using boost::lambda::_1;

            auto serviceToTest = PortInfo.find(proto);
            std::string toTest;
            //check if the port exists in our list of services
            if (serviceToTest == PortInfo.end())
                toTest = "unassigned";
            else
                toTest = serviceToTest->second;

            //if verbose is true, then tell the user we're starting to scan a port
            if (args->Verbose)
                std::cout << "Starting thread for port " + std::to_string(currentPort.Port) + "  " + toTest + "\n";

            boost::asio::io_service ioservice;
            client client(ioservice, args->Target, std::to_string(currentPort.Port), args->Timeout);
            //start the connection
            ioservice.run();

            //this is for the progress bar
            if (!args->Verbose)
                scannedPorts->push(currentPort);

            //if the connection worked, add the port to workingports
            if (client.error != boost::asio::error::timed_out)
                workingPorts->push(structures::_port(currentPort.Port, (client.error == boost::system::error_code() ?
                                                                       structures::port_state::port_open :
                                                                       structures::port_state::port_closed)));

        } catch (const std::runtime_error &e){

            if (args->Verbose)
                std::cout << proto + " is not open : " + e.what() + "\n";
        }
        currentPort = portsToCheck->pop();
    }

}
#endif //FINAL_PROJECT_NETWORKCALLS_CPP