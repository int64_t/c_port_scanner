/*
 *  Author  : Philippe Pitz Clairoux
 *  Date    : 14 mai 2018
 *  Details : This file holds the definitions of the prototypes of utils.h . It also holds the maps wich have all the
 *  arguments that can be called and their numeric value.
 */
#ifndef FINAL_PROJECT_UTILS_CPP
#define FINAL_PROJECT_UTILS_CPP

#ifdef __GNUC__
#define nix true
#else
#define win true
#endif
#define _WIDTH 30



#include <iostream>
#include <iomanip>
#include <fstream>
#include <regex>


#include "networkCalls.h"
#include "utils.h"

//value of each argument
enum argument_id {
        target = 2,
        ports = 3,
        help = 4,
        verbose = 5,
        set_timeout=6,
        use_default_ports=7,
        skip_warning=8
    };
//name of arg and the max amount of args that can be next to it
static std::map<std::string, int64_t> Arguments;

static std::map<std::string, std::string> argumentsDescriptions;

//add stuff to the maps and catch exceptions if there's one (mainly std::bad_alloc)
void feed_data() {
    try{
        argumentsDescriptions =  {{"--target",            "--target <target> : Specefy a target to scan ports (ip or website)"},
                                 {"--ports",             "--ports <...> : Scan multiple ports (space between each port)"},
                                 {"--help",              "--help : Display this list"},
                                 {"--verbose",           "--verbose : Make the program verbose (print more details on the screen)"},
                                 {"--use-default-ports", "--use-default-ports : Use default ports instead of iterating from 1 to 49150"},
                                 {"--skip-warning",      "--skip-warning : Skips the warning at the start of the program"},
                                 {"--set-timeout", "--set-timeout : Set's a timeout on a socket (default is 1 second)"}};

        Arguments = {{"--target",           argument_id::target},
                    {"--ports",             argument_id::ports},
                    {"--help",              argument_id::help},
                    {"--verbose",           argument_id::verbose},
                    {"--use-default-ports", argument_id::use_default_ports},
                    {"--skip-warning",      argument_id::skip_warning},
                    {"--set-timeout",       argument_id::set_timeout}};
    } catch (std::exception &e){

        throw;
    }
}

//This function is used to make the program exit
[[ noreturn ]] void utils::c_exit(const std::string& message, int errorCode) noexcept{
#if nix
  std::cout << message << std::endl;
  exit(errorCode);
#elif defined(win)
  std::cout << message << std::endl;
  system("pause");
  exit(errorCode);
#else
  exit(-1);
#endif
}
//display arguments
void display_args() noexcept {

  int i = 0;
  std::cout << "USAGE : COMMAND [argument(s)] " << std::endl;
  for (auto const &value : Arguments) {
    auto description = argumentsDescriptions.find(value.first);
    std::cout << "  " << description->second << std::endl;
    ++i;
  }

}
//help message
inline void help_message() noexcept {

  std::cerr << "You must pass at least one argument" << std::endl;
  std::cerr << "example : COMMAND --target duckduckgo.com" << std::endl;
  std::cerr << "example : COMMAND --target google.com --ports 22 25 30 443 8080" << std::endl;
  std::cerr << "example : COMMAND --target facebook.com --use-default-ports" << std::endl;
  std::cerr << "Use the --help flag for more info." << std::endl;

}

//remove useless spaces from the string by doing this
inline std::string remove_spaces(const std::string &buffer) noexcept {

  std::stringstream tmp(buffer);
  std::string tmpString;

  tmp >> tmpString;

  return tmpString;
}
//shove ports from a string to a vector
template<typename T>
void get_ports(const std::string &ports, std::vector<T> &vector) {

  std::stringstream tmp(ports);
  int tmpInt = 0;

  while (!tmp.eof()) {
    tmp >> tmpInt;
    vector.push_back(tmpInt);
    tmpInt = 0;
  }
}
//this function parses args
void utils::arg_parser(int argc, char **argv, structures::ParsedArgs &arg) {

  //if there's less than 2 args, throw an an error
  if (argc < 2) {

      help_message();
    throw std::invalid_argument("Invalid amount of argument");
  }

  //add data to the maps
    feed_data();

  std::map<int64_t, std::string> Segments;
  structures::ParsedArgs *args = &arg;
  std::string tmp;
  std::string buffer;
  std::regex reg("(--(?:\\w|\\-)+)(.*?)(?=--|$)");

  //add all flags and arguments to the buffer to use the regex on it afterwards
  for (int i = 1; i < argc; i++)
    buffer += std::string(" " + std::string(argv[i]));

  std::regex_iterator<std::string::iterator> regexIterator(buffer.begin(), buffer.end(), reg);
  std::regex_iterator<std::string::iterator> regexEnd;

  //parse every flag and argument one after the other
  while (regexIterator != regexEnd) {

    //if there's an argument is found, start doing stuff with it
    auto foundArg = Arguments.find(regexIterator->str(1));


    if (foundArg->second == 0)
      throw std::invalid_argument("Invalid argument was inputted.");
    //add flag + arguments to the map
    Segments.insert({foundArg->second, std::string(regexIterator->str(2))});


    ++regexIterator;
  }

  //for every flag, parse them
  for (auto value : Segments) {

    switch (value.first) {

      case argument_id::target :
        args->Target = remove_spaces(value.second);
        break;
      case argument_id::ports :
        get_ports(value.second, args->Ports);
        break;
      case argument_id::help :
        display_args();
        utils::c_exit("", 0);
        break;  
      case argument_id::verbose :
        args->Verbose = true;
        break;
      case argument_id::set_timeout:
        args->Timeout = (uint16_t) stoi(value.second);
        break;
      case argument_id::use_default_ports:
        if (!args->Ports.empty())
            throw std::invalid_argument("Cannot use --use-default-ports if --ports is called.");
        args->UseDefaultPorts = true;
        break;
      case argument_id::skip_warning:
        args->SkipWarning = true;
        break;
      default:
        display_args();
        throw std::invalid_argument("Invalid argument");
    }
  }

}

//load all the ports from the file to a map
void utils::load_ports(std::map<std::string, std::string> &toFill, const std::string &FileName){

    std::ifstream stream;
    stream.open(FileName);

    if (stream.fail() && !stream.is_open())
        throw std::invalid_argument("Cannot open the port file.");

    while (!stream.eof()){

        std::string desc, name;

        stream >> name >> desc;

        toFill.insert({desc, name});
    }

    assert(!toFill.empty() &&
           "Couldn't load any port. Make sure ports.txt is next to the executable of this program");
}
//this is to display a progress bar
void utils::progress_bar(structures::PortQueue *scannedPorts, const unsigned long &amountPorts) noexcept {
    float progress;

    do {
        progress =  (float) scannedPorts->queueSize() / amountPorts;
        std::cout << "[";
        auto pos = int(_WIDTH * progress);
        for (int i = 0; i < _WIDTH;++i){
            if (i <= pos) std::cout << "#";
            else std::cout << "-";
        }
        std::cout << "] " << int(progress * 100.0) << " %\r";
        std::cout.flush();

    }while (1.0 != progress);
    std::cout << std::endl;
}


#endif //FINAL_PROJECT_UTILS_H

