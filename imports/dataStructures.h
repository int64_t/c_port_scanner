/*
 *  Author  : Philippe Pitz Clairoux
 *  Date    : 14 mai 2018
 *  Details : This file contains every data structure used in the program. It holds the definition of the
 *  struct that holds information about every port, an enum for port states, the definition of PortQueue
 *  (queue with mutexes) and as well as the struct to hold the data of the passed arguments.
 */
#ifndef FINAL_PROJECT_DATASTRUCTURES_H
#define FINAL_PROJECT_DATASTRUCTURES_H


#include <queue>
#include <string>
#include <vector>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lockable_adapter.hpp>
#include <boost/thread/condition_variable.hpp>

namespace structures {

    enum port_state {
        default_value = -1,
        port_open = 0,
        port_closed = 1,
    };

    struct _port {
        unsigned short Port = 0;
        port_state State = port_state::default_value;

        explicit _port(unsigned short p=0, port_state s=port_state::default_value){
            Port = p;
            State = s;
        }
    };

/*
* This class will be used ALOT in the program. It's a queue that holds ports.
* It has a few methods defined : push, pop, queueSize and run.
* All the methods mentionned aboved are implemented with mutexs in order to make sure
* we dont have different threads using the same object at the same time (to avoid run time errors, obviously).
*/
    class PortQueue {
    public:
        //make readyToStart to true and let the threads start
        void run() {

            boost::lock_guard<boost::mutex> lock(this->mut);

            this->readyToStart = true;

            this->cv.notify_one();
        }
        //get a port
        _port pop() {

            boost::mutex::scoped_lock lock(this->mut);

            while (!this->readyToStart)
                cv.wait(lock);
            _port tmp;
            if (!this->toScan.empty()) {
                tmp = this->toScan.front();
                this->toScan.pop();
            } else
                tmp.Port = 0;

            this->cv.notify_one();

            return tmp;
        }
        //get the size of the queue
        unsigned long queueSize() {
            boost::lock_guard<boost::mutex> lock(this->mut);

            unsigned long tmp = this->toScan.size();

            this->cv.notify_one();

            return tmp;
        }
        //add a new number to the queue
        void push(_port t) {

            boost::mutex::scoped_lock lock(this->mut);

            this->toScan.push(t);

            this->cv.notify_one();
        }

    private:
        std::queue<_port> toScan;
        boost::mutex mut;
        bool readyToStart = false;
        boost::condition_variable cv;
    };



    struct ParsedArgs {

        bool UseDefaultPorts = false;
        bool SkipWarning = false;
        bool Verbose = false;

        uint16_t Timeout = 1;

        std::vector<unsigned short> Ports;

        std::string Target = "";
        std::string Protocol = "tcp";
    };


    //https://web.archive.org/web/20160730024945/https://www.securecoding.cert.org/confluence/display/
    //cplusplus/ERR60-CPP.+Exception+objects+must+be+nothrow+copy+constructible
    struct runtimeError : std::runtime_error {
        explicit runtimeError(const char *msg) : std::runtime_error(msg) {};
    };


}
#endif //FINAL_PROJECT_DATASTRUCTURES_H
